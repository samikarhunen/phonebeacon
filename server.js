var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000,
  mongoose = require('mongoose'),

  //tietokantamalli
  Task = require('./api/models/visitorModel'),
  bodyParser = require('body-parser');

mongoose.Promise = global.Promise;

//tietokanta???
mongoose.connect('mongodb://localhost/PhoneBeaconDatabase');


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


var routes = require('./api/routes/routes');
routes(app);

app.use(function(req, res) {
  res.status(404).send({url: req.originalUrl + ' not found'})
});

app.listen(port);

console.log('Visitor counting microservice started on: ' + port);
