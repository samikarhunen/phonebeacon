'use strict';


var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var visitorSchema = new Schema({
  beaconid: {
    type: String,
    required: [true, 'Beaconid required']
  },
  visitedtime: {
    type: Date,
    default: Date.now
  },
  phoneid: {
    type: String,
    required: [true, 'User phoneid required']
  },
});



module.exports = mongoose.model('PhoneBeaconModel', visitorSchema, 'PhoneBeaconDatabase');