'use strict';

module.exports = function(app) {
	var visitorList = require('../controllers/visitorController');

	// Routes
	app.route('/visitors')
		.get(visitorList.list_all_visitors)
		.post(visitorList.create_a_visitor);

};
