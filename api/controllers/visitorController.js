'use strict';

var mongoose = require('mongoose'),
  Visitor = mongoose.model('PhoneBeaconModel');



exports.list_all_visitors = function(req, res) {
  Visitor.find({}, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};


exports.create_a_visitor = function(req, res) {
  var new_visitor = new Visitor(req.body);
  new_visitor.save(function(err, task) {
    if (err)
      res.send(err);
      console.log("Errors: " + err);
    res.json(task);
  });
};


